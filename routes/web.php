<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);


//ADMIN
//Beranda
Route::resource('admin/beranda', 'Web\Admin\BerandaController', ['as' => 'admin']);
//Golongan
Route::resource('admin/golongan', 'Web\Admin\GolonganController', ['as' => 'admin']);
//Pendidikan
Route::resource('admin/pendidikan', 'Web\Admin\PendidikanController', ['as' => 'admin']);
//Jabatan
Route::resource('admin/jabatan', 'Web\Admin\JabatanController', ['as' => 'admin']);
//Pegawai
Route::resource('admin/pegawai', 'Web\Admin\PegawaiController', ['as' => 'admin']);
//History
Route::resource('admin/history', 'Web\Admin\HistoryController', ['as' => 'admin']);
//Izin
Route::resource('admin/izin', 'Web\Admin\IzinController', ['as' => 'admin']);
Route::post('admin/izin/cetak', 'Web\Admin\IzinController@cetak')->name('admin.izin.cetak');
//Cuti
Route::resource('admin/cuti', 'Web\Admin\CutiController', ['as' => 'admin']);
Route::post('admin/cuti/cetak', 'Web\Admin\CutiController@cetak')->name('admin.cuti.cetak');
//Naik
Route::resource('admin/naikjabatan', 'Web\Admin\NaikController', ['as' => 'admin']);
//Mutasi
Route::resource('admin/mutasi', 'Web\Admin\MutasiController', ['as' => 'admin']);
//Gaji
Route::resource('admin/gaji', 'Web\Admin\GajiController', ['as' => 'admin']);
Route::post('admin/gaji/cetak', 'Web\Admin\GajiController@cetak')->name('admin.gaji.cetak');
//Absensi
Route::resource('admin/absensi', 'Web\Admin\AbsensiController', ['as' => 'admin']);
Route::post('admin/absensi/cetak', 'Web\Admin\AbsensiController@cetak')->name('admin.absensi.cetak');




//PIMPINAN
//Beranda
Route::resource('pimpinan/beranda', 'Web\Pimpinan\BerandaController', ['as' => 'pimpinan']);
//Pegawai
Route::resource('pimpinan/pegawai', 'Web\Pimpinan\PegawaiController', ['as' => 'pimpinan']);
Route::get('pimpinan/pegawai/cetak/{id}', 'Web\Pimpinan\PegawaiController@cetak')->name('pimpinan.pegawai.cetak');
Route::get('cetak-semua-pegawai', 'Web\Pimpinan\PegawaiController@cetak_all')->name('cetak-semua-pegawai');



//PEGAWAI
//Beranda
Route::resource('pegawai/beranda', 'Web\Pegawai\BerandaController', ['as' => 'pegawai']);
//Cuti
Route::resource('pegawai/cuti', 'Web\Pegawai\CutiController', ['as' => 'pegawai']);
