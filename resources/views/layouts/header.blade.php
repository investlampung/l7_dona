<header class="main-header">
    <a href="{{ asset('admin/beranda') }}" class="logo">
        <span class="logo-mini">PTPN</span>
        <span class="logo-lg"><b>PTPN</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Auth::user()->jk==='Laki - Laki')
                        <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="user-image" alt="User Image">
                        @else
                        <img src="{{ asset('itlabil/admin/dist/img/avatar3.png') }}" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(Auth::user()->jk==='Laki - Laki')
                            <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
                            @else
                            <img src="{{ asset('itlabil/admin/dist/img/avatar3.png') }}" class="img-circle" alt="User Image">
                            @endif
                            <p>{{Auth::user()->name}}</p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div align="center">
                                <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>