<li><a href="{{ asset('admin/beranda') }}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
<!-- <li class="treeview">
    <a href="#">
        <i class="fa fa-database"></i>
        <span>Data Master</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/golongan') }}"><i class="fa fa-circle-o"></i> Golongan</a></li>
        <li><a href="{{ asset('admin/jabatan') }}"><i class="fa fa-circle-o"></i> Jabatan</a></li>
        <li><a href="{{ asset('admin/pendidikan') }}"><i class="fa fa-circle-o"></i> Pendidikan</a></li>
    </ul>
</li> -->
<li><a href="{{ asset('admin/pegawai') }}"><i class="fa fa-group"></i> <span>Pegawai</span></a></li>
<li><a href="{{ asset('admin/absensi') }}"><i class="fa fa-calendar"></i> <span>Absensi</span></a></li>
<li><a href="{{ asset('admin/gaji') }}"><i class="fa fa-money"></i> <span>Gaji</span></a></li>
<li><a href="{{ asset('admin/izin') }}"><i class="fa fa-check"></i> <span>Izin</span></a></li>
<li><a href="{{ asset('admin/cuti') }}"><i class="fa fa-calendar"></i> <span>Cuti</span></a></li>
<li><a href="{{ asset('admin/naikjabatan') }}"><i class="fa fa-line-chart"></i> <span>Naik Jabatan</span></a></li>
<li><a href="{{ asset('admin/mutasi') }}"><i class="fa fa-suitcase"></i> <span>Mutasi</span></a></li>
<!-- <li><a href="{{ asset('admin/history') }}"><i class="fa fa-list"></i> <span>History</span></a></li> -->