@extends('layouts.admin')
@section('title', 'Pegawai - Cuti')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Cuti
        </h1>
    </section><br><br>

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Data Cuti</h3>
            </div>
            <div class="box-body" style="overflow-x:auto;">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tgl Mulai</th>
                            <th>Tgl Selesai</th>
                            <th>Nama Relasi</th>
                            <th>Jenis Cuti</th>
                            <th>Keterangan</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $total_day=0;
                        @endphp
                        @foreach($data as $item)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $item->tanggal_mulai->format('d-m-Y')}}</td>
                            <td>{{ $item->tanggal_selesai->format('d-m-Y')}}</td>
                            <td>{{ $item->nama_relasi}}</td>
                            <td>{{ $item->jenis_izin}}</td>
                            <td>{{ $item->keterangan}}</td>
                            @php
                            $date1 = $item->tanggal_mulai->format('Y-m-d');
                            $date2 = $item->tanggal_selesai->format('Y-m-d');
                            $datetime1 = new DateTime($date1);
                            $datetime2 = new DateTime($date2);
                            $interval = $datetime1->diff($datetime2);
                            $days = $interval->format('%a');
                            $jml_day = $days+1;
                            $total_day = $total_day+$jml_day;
                            @endphp
                            <td>{{ $jml_day }} Hari</td>
                        </tr>
                        @endforeach
                        @php
                        $kuota=15-$total_day;
                        @endphp
                        <tr>
                            <td colspan="7"><br><b>KUOTA CUTI : {{$kuota}} Hari</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection