@extends('layouts.admin')
@section('title', 'Admin - Pendidikan')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Pendidikan
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Pendidikan</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.pendidikan.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Pendidikan</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{$data->pendidikan}}" name="pendidikan" class="form-control" placeholder="Pendidikan">
                                    <small class="text-danger">{{ $errors->first('pendidikan') }}</small>
                                </div>
                            </div>

                            <a href="{{url('admin/pendidikan')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection