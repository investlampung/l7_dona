@extends('layouts.admin')
@section('title', 'Admin Beranda')
@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Beranda
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12" align="center">
            <img src="{{asset('itlabil/image/default/logo.png')}}" width="300px"><br>
            <h1>PT. PERKEBUNAN NUSANTARA VII</h1>
            AFDELING IV<br>
            Unit Wayberulu Desa Sukaraja Kec. Gedongtataan 35366<br>
            PESAWARAN
        </div>
    </div>
</div>

@endsection