@extends('layouts.admin')
@section('title', 'Admin - Izin')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Izin Pegawai
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin.izin.cetak') }}" class="form-horizontal" method="POST" target="_blank">
                @csrf

                <div class="col-sm-2">
                    <select class="form-control" name="tahun">
                        {{ $last= date('Y')-3 }}
                        {{ $now = date('Y') }}

                        @for ($i = $now; $i >= $last; $i--)
                        <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" name="bulan">
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Cetak</button>
            </form>
            <br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Izin Pegawai</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.izin.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="user_id">
                                        @foreach($pegawai as $pe)
                                        <option value="{{$pe->id}}">{{$pe->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Mulai</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal_mulai" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal_mulai') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Selesai</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal_selesai" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal_selesai') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Keperluan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="keperluan" class="form-control" placeholder="Keperluan">
                                    <small class="text-danger">{{ $errors->first('keperluan') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Relasi</label>

                                <div class="col-sm-8">
                                    <input type="text" name="nama_relasi" class="form-control" placeholder="Relasi">
                                    <small class=" text-danger">{{ $errors->first('nama_relasi') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Tgl Mulai</th>
                                <th>Tgl Selesai</th>
                                <th>Alamat</th>
                                <th>Keperluan</th>
                                <th>Relasi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->user->nip}}</td>
                                <td>{{ $item->user->name}}</td>
                                <td>{{ $item->tanggal_mulai->format('d-m-Y')}}</td>
                                <td>{{ $item->tanggal_selesai->format('d-m-Y')}}</td>
                                <td>{{ $item->user->alamat}}</td>
                                <td>{{ $item->keperluan}}</td>
                                <td>{{ $item->nama_relasi}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.izin.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.izin.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection