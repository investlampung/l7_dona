@extends('layouts.admin')
@section('title', 'Admin - Naik Jabatan')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Naik Jabatan
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Naik Jabatan</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.naikjabatan.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="user_id">
                                        @foreach($pegawai as $pe)
                                        <option value="{{$pe->id}}">{{$pe->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Jabatan Lama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jabatan_lama">
                                        @foreach($jabatan as $jab)
                                        <option value="{{$jab->jabatan}}">{{$jab->jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Jabatan Baru</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jabatan_baru">
                                        @foreach($jabatan as $jab)
                                        <option value="{{$jab->jabatan}}">{{$jab->jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Keterangan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="keterangan" class="form-control" placeholder="Keterangan">
                                    <small class="text-danger">{{ $errors->first('keterangan') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jabatan Lama</th>
                                <th>Jabatan BAru</th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->user->name}}</td>
                                <td>{{ $item->jabatan_lama}}</td>
                                <td>{{ $item->jabatan_baru}}</td>
                                <td>{{ $item->tanggal}}</td>
                                <td>{{ $item->keterangan}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.naikjabatan.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.naikjabatan.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection