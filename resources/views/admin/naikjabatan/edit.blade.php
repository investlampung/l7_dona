@extends('layouts.admin')
@section('title', 'Admin - Naik Jabatan')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Naik Jabatan
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Naik Jabatan</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.naikjabatan.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="user_id">
                                        <option value="{{$data->user->id}}">{{$data->user->name}}</option>
                                        @foreach($pegawai as $pe)
                                        <option value="{{$pe->id}}">{{$pe->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Jabatan Lama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jabatan_lama">
                                        <option value="{{$data->jabatan_lama}}">{{$data->jabatan_lama}}</option>
                                        @foreach($jabatan as $jab)
                                        <option value="{{$jab->jabatan}}">{{$jab->jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Jabatan Baru</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jabatan_baru">
                                        <option value="{{$data->jabatan_baru}}">{{$data->jabatan_baru}}</option>
                                        @foreach($jabatan as $jab)
                                        <option value="{{$jab->jabatan}}">{{$jab->jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="date" value="{{$data->tanggal}}" name="tanggal" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Keterangan</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->keterangan}}" name="keterangan" class="form-control" placeholder="Keterangan">
                                    <small class="text-danger">{{ $errors->first('keterangan') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/naikjabatan')}}" class="btn btn-default pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection