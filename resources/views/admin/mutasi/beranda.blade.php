@extends('layouts.admin')
@section('title', 'Admin - Mutasi')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Mutasi
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Mutasi</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.mutasi.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">NIP</label>

                                <div class="col-sm-8">
                                    <input type="text" name="nip" class="form-control" placeholder="NIP">
                                    <small class="text-danger">{{ $errors->first('nip') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <input type="text" name="nama" class="form-control" placeholder="Nama">
                                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Asal</label>

                                <div class="col-sm-8">
                                    <input type="text" name="asal" class="form-control" placeholder="Asal">
                                    <small class="text-danger">{{ $errors->first('asal') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tujuan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="tujuan" class="form-control" placeholder="Tujuan">
                                    <small class="text-danger">{{ $errors->first('tujuan') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Alasan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="alasan" class="form-control" placeholder="Alasan">
                                    <small class="text-danger">{{ $errors->first('alasan') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Asal</th>
                                <th>Tujuan</th>
                                <th>Tanggal</th>
                                <th>Alasan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->nip}}</td>
                                <td>{{ $item->nama}}</td>
                                <td>{{ $item->asal}}</td>
                                <td>{{ $item->tujuan}}</td>
                                <td>{{ $item->tanggal}}</td>
                                <td>{{ $item->alasan}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.mutasi.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.mutasi.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection