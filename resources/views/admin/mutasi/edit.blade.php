@extends('layouts.admin')
@section('title', 'Admin - Mutasi')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Mutasi
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Mutasi</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.mutasi.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">NIP</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->nip}}" name="nip" class="form-control" placeholder="NIP">
                                    <small class="text-danger">{{ $errors->first('nip') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->nama}}" name="nama" class="form-control" placeholder="Nama">
                                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="date" value="{{$data->tanggal}}" name="tanggal" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Asal</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->asal}}" name="asal" class="form-control" placeholder="Asal">
                                    <small class="text-danger">{{ $errors->first('asal') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tujuan</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->tujuan}}" name="tujuan" class="form-control" placeholder="Tujuan">
                                    <small class="text-danger">{{ $errors->first('tujuan') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Alasan</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->alasan}}" name="alasan" class="form-control" placeholder="Alasan">
                                    <small class="text-danger">{{ $errors->first('alasan') }}</small>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/mutasi')}}" class="btn btn-default pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection