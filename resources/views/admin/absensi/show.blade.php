@extends('layouts.admin')
@section('title', 'Admin - Absensi')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Absensi - {{$data->name}}</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->absensi as $item)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $item->tanggal->format('d-m-Y') }}</td>
                                <td>{{$item->ket}}</td>
                                <td>
                                    <form action="{{ route('admin.absensi.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.absensi.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <a href="{{url('admin/absensi')}}" class="btn btn-default">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection