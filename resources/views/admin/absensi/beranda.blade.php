@extends('layouts.admin')
@section('title', 'Admin - Absensi')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('admin.absensi.cetak') }}" class="form-horizontal" method="POST" target="_blank">
                @csrf

                <div class="col-sm-3">
                    <input type="date" name="tanggal" class="form-control" placeholder="Tanggal">
                </div>
                <button type="submit" class="btn btn-primary">Cetak</button>
            </form>
            <br>
        </div>
        <div class="col-md-4" align="right">
            <a href="{{ route('admin.absensi.create') }}" class="btn btn-primary">+ Absensi</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Absensi</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->nip}}</td>
                                <td>{{ $item->name}}</td>
                                <td align="center">
                                    <a class="btn btn-primary" href="{{ route('admin.absensi.show',$item->id) }}">Lihat</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection