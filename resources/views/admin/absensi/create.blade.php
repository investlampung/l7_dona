@extends('layouts.admin')
@section('title', 'Admin - Absensi')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Absensi</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.absensi.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-12">
                            @foreach($data as $item)
                            <div class="form-group">
                                <label class="col-sm-3">{{$item->name}}</label>
                                <input type="hidden" name="tanggal[{{$no}}]" value="<?php echo date('Y-m-d'); ?>" />
                                <input type="hidden" name="user_id[{{$no}}]" value="{{$item->id}}">
                                <div class="col-sm-2">
                                    <input type="radio" id="hadir" name="ket[{{$no}}]" value="Hadir"><label for="Hadir" style="margin-left: 5px;"> Hadir</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" id="izin" name="ket[{{$no}}]" value="Izin"><label for="Izin" style="margin-left: 5px;"> Izin</label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="radio" id="cuti" name="ket[{{$no}}]" value="Cuti"><label for="Cuti" style="margin-left: 5px;"> Cuti</label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="radio" id="thadir" name="ket[{{$no}}]" value="Tidak Hadir"><label for="Tidak Hadir" style="margin-left: 5px;"> Tidak Hadir</label>
                                </div>
                            </div>
                            @php $no++ @endphp
                            @endforeach
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/absensi')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection