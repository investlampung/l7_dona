@extends('layouts.admin')
@section('title', 'Admin - Absensi')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Absensi</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.absensi.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="user_id">
                                        <option value="{{$data->user->id}}">{{$data->user->name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal</label>

                                <div class="col-sm-8">
                                    <input type="date" value="{{$data->tanggal->format('Y-m-d')}}" name="tanggal" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Keterangan</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="ket">
                                        <option value="{{$data->ket}}">{{$data->ket}}</option>
                                        <option value="Hadir">Hadir</option>
                                        <option value="Izin">Izin</option>
                                        <option value="Cuti">Cuti</option>
                                        <option value="Tidak Hadir">Tidak Hadir</option>
                                    </select>
                                </div>
                            </div>

                            <a href="{{url('admin/absensi')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection