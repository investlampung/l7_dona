@extends('layouts.admin')
@section('title', 'Admin - Pegawai')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Pegawai
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Pegawai</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.pegawai.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->name}}" name="name" class="form-control" placeholder="Nama">
                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" value="{{$data->email}}" name="email" class="form-control" placeholder="Email">
                                    <small class="text-danger">{{ $errors->first('email') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nip</label>
                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->nip}}" name="nip" class="form-control" placeholder="NIP">
                                    <small class="text-danger">{{ $errors->first('nip') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tempat Lahir</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->tempat_lahir}}" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                                    <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Lahir</label>

                                <div class="col-sm-8">
                                    <input type="date" value="{{$data->tanggal_lahir}}" name="tanggal_lahir" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Alamat</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->alamat}}" name="alamat" class="form-control" placeholder="Alamat">
                                    <small class="text-danger">{{ $errors->first('alamat') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jenis Kelamin</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="jk">
                                        <option value="{{$data->jk}}">{{$data->jk}}</option>
                                        <option value="Laki - Laki">Laki - Laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Pendidikan</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="pendidikan">
                                        <option value="{{$data->pendidikan}}">{{$data->pendidikan}}</option>
                                        @foreach($pendidikan as $pen)
                                        <option value="{{$pen->pendidikan}}">{{$pen->pendidikan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Golongan</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->golongan}}" name="golongan" class="form-control" placeholder="Golongan">
                                    <small class="text-danger">{{ $errors->first('golongan') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Jabatan</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jabatan">
                                        <option value="{{$data->jabatan}}">{{$data->jabatan}}</option>
                                        @foreach($jabatan as $jab)
                                        <option value="{{$jab->jabatan}}">{{$jab->jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Telp</label>
                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->no_telp}}" name="no_telp" class="form-control" placeholder="No Telp">
                                    <small class="text-danger">{{ $errors->first('no_telp') }}</small>
                                </div>
                            </div>

                            <a href="{{url('admin/pegawai')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection