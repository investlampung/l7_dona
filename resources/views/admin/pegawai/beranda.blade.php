@extends('layouts.admin')
@section('title', 'Admin - Pegawai')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Pegawai
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Pegawai</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.pegawai.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" placeholder="Nama">
                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                    <small class="text-danger">{{ $errors->first('email') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nip</label>
                                <div class="col-sm-8">
                                    <input type="text" name="nip" class="form-control" placeholder="NIP">
                                    <small class="text-danger">{{ $errors->first('nip') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tempat Lahir</label>

                                <div class="col-sm-8">
                                    <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
                                    <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Lahir</label>

                                <div class="col-sm-8">
                                    <input type="date" name="tanggal_lahir" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Alamat</label>

                                <div class="col-sm-8">
                                    <input type="text" name="alamat" class="form-control" placeholder="Alamat">
                                    <small class="text-danger">{{ $errors->first('alamat') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Jenis Kelamin</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="jk">
                                        <option value="Laki - Laki">Laki - Laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Pendidikan</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="pendidikan">
                                        @foreach($pendidikan as $pen)
                                        <option value="{{$pen->pendidikan}}">{{$pen->pendidikan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Golongan</label>

                                <div class="col-sm-8">
                                    <input type="text" name="golongan" class="form-control" placeholder="Golongan">
                                    <small class="text-danger">{{ $errors->first('golongan') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Jabatan</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jabatan">
                                        @foreach($jabatan as $jab)
                                        <option value="{{$jab->jabatan}}">{{$jab->jabatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">No Telp</label>
                                <div class="col-sm-8">
                                    <input type="text" name="no_telp" class="form-control" placeholder="No Telp">
                                    <small class="text-danger">{{ $errors->first('no_telp') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nip</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Golongan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->nip}}</td>
                                <td>{{ $item->name}}</td>
                                <td>{{ $item->jabatan}}</td>
                                <td>{{ $item->golongan}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.pegawai.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-primary" href="{{ route('admin.pegawai.show',$item->id) }}">Lihat</a>
                                        <a class="btn btn-success" href="{{ route('admin.pegawai.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection