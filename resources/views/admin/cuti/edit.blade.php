@extends('layouts.admin')
@section('title', 'Admin - Cuti')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Cuti
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Cuti</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.cuti.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="user_id">
                                        <option value="{{$data->user->id}}">{{$data->user->name}}</option>
                                        @foreach($pegawai as $pe)
                                        <option value="{{$pe->id}}">{{$pe->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Mulai</label>

                                <div class="col-sm-8">
                                    <input type="date" value="{{$data->tanggal_mulai->format('Y-m-d')}}" name="tanggal_mulai" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal_mulai') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Tanggal Selesai</label>

                                <div class="col-sm-8">
                                    <input type="date" value="{{$data->tanggal_selesai->format('Y-m-d')}}" name="tanggal_selesai" class="form-control" placeholder="Tanggal">
                                    <small class="text-danger">{{ $errors->first('tanggal_selesai') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Nama Relasi</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->nama_relasi}}" name="nama_relasi" class="form-control" placeholder="Nama Relasi">
                                    <small class="text-danger">{{ $errors->first('nama_relasi') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Jenis Cuti</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="jenis_izin">
                                        <option value="{{$data->jenis_izin}}">{{$data->jenis_izin}}</option>
                                        <option value="Harian">Harian</option>
                                        <option value="Bulanan">Bulanan</option>
                                        <option value="Tahunan">Tahunan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Keterangan</label>

                                <div class="col-sm-8">
                                    <input type="text" value="{{$data->keterangan}}" name="keterangan" class="form-control" placeholder="Keterangan">
                                    <small class="text-danger">{{ $errors->first('keterangan') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/cuti')}}" class="btn btn-default pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection