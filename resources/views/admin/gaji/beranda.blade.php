@extends('layouts.admin')
@section('title', 'Admin - Gaji')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Gaji Pegawai
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin.gaji.cetak') }}" class="form-horizontal" method="POST" target="_blank">
                @csrf

                <div class="col-sm-2">
                    <select class="form-control" name="tahun">
                        {{ $last= date('Y')-3 }}
                        {{ $now = date('Y') }}

                        @for ($i = $now; $i >= $last; $i--)
                        <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
                <div class="col-sm-2">
                    <select class="form-control" name="bulan">
                        <option value="Januari">Januari</option>
                        <option value="Februari">Februari</option>
                        <option value="Maret">Maret</option>
                        <option value="April">April</option>
                        <option value="Mei">Mei</option>
                        <option value="Juni">Juni</option>
                        <option value="Juli">Juli</option>
                        <option value="Agustus">Agustus</option>
                        <option value="September">September</option>
                        <option value="Oktober">Oktober</option>
                        <option value="November">November</option>
                        <option value="Desember">Desember</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Cetak</button>
            </form>
            <br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Gaji Pegawai</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.gaji.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Nama</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="user_id">
                                        @foreach($pegawai as $pe)
                                        <option value="{{$pe->id}}">{{$pe->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Tahun</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="tahun">
                                        {{ $last= date('Y')-3 }}
                                        {{ $now = date('Y') }}

                                        @for ($i = $now; $i >= $last; $i--)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-4 control-label">Bulan</label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="bulan">
                                        <option value="Januari">Januari</option>
                                        <option value="Februari">Februari</option>
                                        <option value="Maret">Maret</option>
                                        <option value="April">April</option>
                                        <option value="Mei">Mei</option>
                                        <option value="Juni">Juni</option>
                                        <option value="Juli">Juli</option>
                                        <option value="Agustus">Agustus</option>
                                        <option value="September">September</option>
                                        <option value="Oktober">Oktober</option>
                                        <option value="November">November</option>
                                        <option value="Desember">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Total Gaji</label>

                                <div class="col-sm-8">
                                    <input type="number" name="gaji" class="form-control" placeholder="Total Gaji">
                                    <small class="text-danger">{{ $errors->first('gaji') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tahun</th>
                                <th>Bulan</th>
                                <th>Nama</th>
                                <th>Gaji</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->tahun}}</td>
                                <td>{{ $item->bulan}}</td>
                                <td>{{ $item->user->name}}</td>
                                <td>{{ $item->gaji}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.gaji.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-primary" href="{{ route('admin.gaji.show',$item->id) }}" target="_blank">Cetak</a>
                                        <a class="btn btn-success" href="{{ route('admin.gaji.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection