<!DOCTYPE html>
<html>

<head>
    <title>Data Gaji - {{$data->user->name}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }

        .hr4 {
            border: solid 1px;
        }
    </style>
</head>

<body onload="window.print()">
    <!-- onload="window.print()" -->
    <div class="container">
        <table width="100%" border=1 style="margin-bottom: 20px;">
            <tbody>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="right" width="25%">
                                    <img src="{{asset('itlabil/image/default/logo.png')}}" alt="" width="100px">
                                </td>
                                <td align="center" width="650%">
                                    <b>
                                        <h4>PT. PERKEBUNAN NUSANTARA VII</h4>
                                        Perincian Gaji Take Home Pay<br>
                                    </b>
                                </td>
                                <td width="25%"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 5px;">
                        <table width="100%" style="margin: 5px 5px;">
                            <tr>
                                <td width="25%">DETAIL</td>
                                <td width="20%">: GAJI BULANAN</td>
                                <td width="20%">Nama</td>
                                <td width="35%">: {{$data->user->name}}</td>
                            </tr>
                            <tr>
                                <td>Bulan</td>
                                <td>: {{$data->created_at->format('m/Y')}}</td>
                                <td>NIK SAP</td>
                                <td>: {{$data->user->nip}}</td>
                            </tr>
                            <tr>
                                <td>Status Kar</td>
                                <td>: Karpel - Tetap</td>
                                <td>Posisi</td>
                                <td>: {{$data->user->jabatan}}</td>
                            </tr>
                            <tr>
                                <td>Gol Kar</td>
                                <td>: {{$data->user->golongan}}</td>
                                <td>Unit Kerja</td>
                                <td>: Wabe-Tanaman-Afdeling 4</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 5px;">
                        <table width="100%" style="margin: 5px 5px;">
                            <tr>
                                <td width="75%">TOTAL GAJI</td>
                                <td>Rp. {{ number_format($data->gaji, 0, ".", ".")}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 5px;">
                        <table width="100%" style="margin: 5px 5px;">
                            <tr>
                                <td width="30%" align="center">
                                    PENERIMA,<br><br><br><br>
                                    {{$data->user->name}}
                                </td>
                                <td width="40%"></td>
                                <td width="30%" align="center" valign="top">PTPN VII Unit Wabe</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

</body>

</html>