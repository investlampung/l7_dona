@extends('layouts.admin')
@section('title', 'Admin - Jabatan')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Jabatan
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Jabatan</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.jabatan.update', $data->id) }}" class="form-horizontal" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jabatan</label>

                                <div class="col-sm-10">
                                    <input type="text" value="{{$data->jabatan}}" name="jabatan" class="form-control" placeholder="Jabatan">
                                    <small class="text-danger">{{ $errors->first('jabatan') }}</small>
                                </div>
                            </div>

                            <a href="{{url('admin/jabatan')}}" class="btn btn-default">Kembali</a>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection