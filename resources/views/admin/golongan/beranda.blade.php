@extends('layouts.admin')
@section('title', 'Admin - Golongan')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Golongan
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Golongan</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.golongan.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Golongan</label>

                                <div class="col-sm-10">
                                    <input type="text" name="golongan" class="form-control" placeholder="Golongan">
                                    <small class="text-danger">{{ $errors->first('golongan') }}</small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
                <hr>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Golongan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->golongan}}</td>
                                <td align="center">
                                    <form action="{{ route('admin.golongan.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.golongan.edit',$item->id) }}">Ubah</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection