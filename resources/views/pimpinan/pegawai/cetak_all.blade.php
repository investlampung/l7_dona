<!DOCTYPE html>
<html>

<head>
    <title>Data Pegawai</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }

        .hr4 {
            border: solid 1px;
        }
    </style>
</head>

<body onload="window.print()">
    <!-- onload="window.print()" -->
    <div class="container">
        <table width="100%" style="margin-bottom: 20px;">
            <tbody>
                <tr>
                    <td align="center" width="120px">
                        <img src="{{asset('itlabil/image/default/logo.png')}}" alt="" width="100px">
                    </td>
                    <td align="center">
                        <b>
                            <h4>PT. PERKEBUNAN NUSANTARA VII</h4>
                            AFDELING IV<br>
                            Unit Wayberulu Desa Sukaraja Kec. Gedongtataan 35366<br>
                            PESAWARAN
                        </b>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr class="hr4">
        <table width="100%" style="margin-bottom: 20px;">
            <tbody>
                <tr>
                    <td>Data Pegawai</td>
                </tr>
            </tbody>
        </table>
        <table border="1" width="100%" style="margin-top: 20px;">
            <thead>
                <tr>
                    <th style="padding-left: 10px;">No</th>
                    <th style="padding-left: 10px;">NIP</th>
                    <th style="padding-left: 10px;">Nama</th>
                    <th style="padding-left: 10px;">Jabatan</th>
                    <th style="padding-left: 10px;">No Telp</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $item)
                <tr>
                    <td style="padding-left: 10px;">{{ $no++ }}</td>
                    <td style="padding-left: 10px;">{{ $item->nip}}</td>
                    <td style="padding-left: 10px;">{{ $item->name}}</td>
                    <td style="padding-left: 10px;">{{ $item->jabatan}}</td>
                    <td style="padding-left: 10px;">{{ $item->no_telp}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>

</html>