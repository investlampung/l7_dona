@extends('layouts.admin')
@section('title', 'Pimpinan - Pegawai')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Pegawai
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-primary" href="{{ url('cetak-semua-pegawai') }}" target="_blank">Cetak Semua</a>
            <br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Pegawai</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nip</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Golongan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->nip}}</td>
                                <td>{{ $item->name}}</td>
                                <td>{{ $item->jabatan}}</td>
                                <td>{{ $item->golongan}}</td>
                                <td align="center">
                                    <a class="btn btn-primary" href="{{ route('pimpinan.pegawai.show',$item->id) }}">Lihat</a>
                                    <a class="btn btn-success" href="{{ url('pimpinan/pegawai/cetak',$item->id) }}" target="_blank">Cetak</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection