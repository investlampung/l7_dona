<!DOCTYPE html>
<html>

<head>
    <title>Data Pegawai - {{$data->name}}</title>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        @media print {
            @page {
                size: auto !important
            }
        }

        .samping {
            padding: 15px 15px;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            margin: 15px 15px;
        }

        .kanankiri {
            text-align: justify;
        }

        .tengah {
            text-align: center;
        }

        .hr4 {
            border: solid 1px;
        }
    </style>
</head>

<body onload="window.print()">
    <!-- onload="window.print()" -->
    <div class="container">
        <table width="100%" style="margin-bottom: 20px;">
            <tbody>
                <tr>
                    <td align="center" width="120px">
                        <img src="{{asset('itlabil/image/default/logo.png')}}" alt="" width="100px">
                    </td>
                    <td align="center">
                        <b>
                            <h4>PT. PERKEBUNAN NUSANTARA VII</h4>
                            AFDELING IV<br>
                            Unit Wayberulu Desa Sukaraja Kec. Gedongtataan 35366<br>
                            PESAWARAN
                        </b>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr class="hr4">
        <table width="100%" style="margin-top: 20px;">
            <tbody>
                <tr>
                    <td width="20%">Nama</td>
                    <td>: {{$data->name}}</td>
                </tr>
                <tr>
                    <td>NIP</td>
                    <td>: {{$data->nip}}</td>
                </tr>
                <tr>
                    <td>Tempat & Tanggal Lahir</td>
                    <td>: {{$data->tempat_lahir}}, {{$data->tanggal_lahir}}</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>: {{$data->jk}}</td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td>: {{$data->jabatan}}</td>
                </tr>
                <tr>
                    <td>Golongan</td>
                    <td>: {{$data->golongan}}</td>
                </tr>
                <tr>
                    <td>Pendidikan</td>
                    <td>: {{$data->pendidikan}}</td>
                </tr>
                <tr>
                    <td>No Telp</td>
                    <td>: {{$data->no_telp}}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>: {{$data->email}}</td>
                </tr>
            </tbody>
        </table>
    </div>

</body>

</html>