@extends('layouts.admin')
@section('title', 'Pimpinan - Pegawai')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Pegawai
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Pegawai - {{$data->name}}</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td>{{$data->name}}</td>
                            </tr>
                            <tr>
                                <td>NIP</td>
                                <td>{{$data->nip}}</td>
                            </tr>
                            <tr>
                                <td>Tempat & Tanggal Lahir</td>
                                <td>{{$data->tempat_lahir}}, {{$data->tanggal_lahir}}</td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>{{$data->jk}}</td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td>{{$data->jabatan}}</td>
                            </tr>
                            <tr>
                                <td>Golongan</td>
                                <td>{{$data->golongan}}</td>
                            </tr>
                            <tr>
                                <td>Pendidikan</td>
                                <td>{{$data->pendidikan}}</td>
                            </tr>
                            <tr>
                                <td>No Telp</td>
                                <td>{{$data->no_telp}}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{$data->email}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <a href="{{url('pimpinan/pegawai')}}" class="btn btn-default">Kembali</a>
                    <a class="btn btn-success" href="{{ url('pimpinan/pegawai/cetak',$data->id) }}" target="_blank">Cetak</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection