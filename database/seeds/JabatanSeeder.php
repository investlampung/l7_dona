<?php

use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Jabatan::create(['jabatan' => 'Sinder Afdeling',]);
        App\Jabatan::create(['jabatan' => 'Mandor Besar',]);
        App\Jabatan::create(['jabatan' => 'Krani',]);
        App\Jabatan::create(['jabatan' => 'Mandor',]);
        App\Jabatan::create(['jabatan' => 'Keamanan',]);
        App\Jabatan::create(['jabatan' => 'Penyadap',]);
    }
}
