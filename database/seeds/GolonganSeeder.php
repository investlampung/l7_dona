<?php

use Illuminate\Database\Seeder;

class GolonganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Golongan::create(['golongan' => '1A',]);
        App\Golongan::create(['golongan' => '1B',]);
        App\Golongan::create(['golongan' => '1C',]);
        App\Golongan::create(['golongan' => '1D',]);
        App\Golongan::create(['golongan' => '2A',]);
        App\Golongan::create(['golongan' => '2B',]);
        App\Golongan::create(['golongan' => '2C',]);
        App\Golongan::create(['golongan' => '2D',]);
        App\Golongan::create(['golongan' => '3A',]);
        App\Golongan::create(['golongan' => '3B',]);
        App\Golongan::create(['golongan' => '3C',]);
        App\Golongan::create(['golongan' => '3D',]);
    }
}
