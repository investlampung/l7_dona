<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{

    public function run()
    {
        App\User::create([
            'email'         => 'admin@ptpn.id',
            'name'          => 'Administrator',
            'password'      => bcrypt('1sampai2'),
            'nip'           => '',
            'tanggal_lahir' => '',
            'tempat_lahir'  => '',
            'alamat'        => '',
            'jk'            => '',
            'no_telp'       => '',
            'jabatan'       => '',
            'golongan'      => '',
            'pendidikan'    => '',
            'role'          => 'admin',
        ]);
        App\User::create([
            'email'         => 'pimpinan@ptpn.id',
            'name'          => 'Pimpinan',
            'password'      => bcrypt('1sampai2'),
            'nip'           => '123456789',
            'tanggal_lahir' => '',
            'tempat_lahir'  => '',
            'alamat'        => '',
            'jk'            => '',
            'no_telp'       => '089631073321',
            'jabatan'       => '',
            'golongan'      => '',
            'pendidikan'    => 'Sarjana 1',
            'role'          => 'pimpinan',
        ]);
        App\User::create([
            'email'         => 'pegawai@ptpn.id',
            'name'          => 'Dona Safitri',
            'password'      => bcrypt('1sampai2'),
            'nip'           => '123456784',
            'tanggal_lahir' => '',
            'tempat_lahir'  => 'Gedongtataan',
            'alamat'        => 'Gedongtataan',
            'jk'            => 'Perempuan',
            'no_telp'       => '089631073321',
            'jabatan'       => '',
            'golongan'      => '',
            'pendidikan'    => 'Sarjana 1',
            'role'          => 'pegawai',
        ]);
    }
}
