<?php

use Illuminate\Database\Seeder;

class PendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Pendidikan::create([
            'pendidikan' => '-'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'Tidak Sekolah'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'Putus SD'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'SD Sederajat'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'SMP Sederajat'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'SMA Sederajat'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'D1'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'D2'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'D3'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'S1'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'S2'
        ]);
        App\Pendidikan::create([
            'pendidikan' => 'S3'
        ]);
    }
}
