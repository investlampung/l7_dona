<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(GolonganSeeder::class);
        $this->call(JabatanSeeder::class);
        $this->call(PendidikanSeeder::class);
        $this->call(UserSeeder::class);
    }
}
