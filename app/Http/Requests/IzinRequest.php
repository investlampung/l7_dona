<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IzinRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'user_id'                      => 'required',
                        'tanggal_mulai'                => 'required',
                        'tanggal_selesai'              => 'required',
                        'nama_relasi'                  => 'required',
                        'keperluan'                   => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'user_id'                      => 'required',
                        'tanggal_mulai'                => 'required',
                        'tanggal_selesai'              => 'required',
                        'nama_relasi'                  => 'required',
                        'keperluan'                   => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'user_id.required' => 'Tidak boleh kosong',
            'tanggal_mulai.required' => 'Tidak boleh kosong',
            'tanggal_selesai.required' => 'Tidak boleh kosong',
            'nama_relasi.required' => 'Tidak boleh kosong',
            'keperluan.required' => 'Tidak boleh kosong',
        ];
    }
}
