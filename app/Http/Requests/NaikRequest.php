<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NaikRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'tanggal'              => 'required',
                        'keterangan'           => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'tanggal'                => 'required',
                        'keterangan'             => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'tanggal.required' => 'Tidak boleh kosong',
            'keterangan.required' => 'Tidak boleh kosong',
        ];
    }
}
