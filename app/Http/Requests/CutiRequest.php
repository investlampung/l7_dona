<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CutiRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'tanggal_mulai'                => 'required',
                        'tanggal_selesai'              => 'required',
                        'nama_relasi'                  => 'required',
                        'jenis_izin'                   => 'required',
                        'keterangan'                   => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'tanggal_mulai'                => 'required',
                        'tanggal_selesai'              => 'required',
                        'nama_relasi'                  => 'required',
                        'jenis_izin'                   => 'required',
                        'keterangan'                   => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'tanggal_mulai.required' => 'Tidak boleh kosong',
            'tanggal_selesai.required' => 'Tidak boleh kosong',
            'nama_relasi.required' => 'Tidak boleh kosong',
            'jenis_izin.required' => 'Tidak boleh kosong',
            'keterangan.required' => 'Tidak boleh kosong',
        ];
    }
}
