<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PegawaiRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'name'                     => 'required',
                        'email'                      => 'required',
                        'nip'                      => 'required',
                        'tempat_lahir'             => 'required',
                        'alamat'                   => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'name'                     => 'required',
                        'email'                      => 'required',
                        'nip'                      => 'required',
                        'tempat_lahir'             => 'required',
                        'alamat'                   => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'name.required' => 'Tidak boleh kosong',
            'nip.required' => 'Tidak boleh kosong',
            'tanggal_lahir.required' => 'Tidak boleh kosong',
            'tempat_lahir.required' => 'Tidak boleh kosong',
            'alamat.required' => 'Tidak boleh kosong',
            'email.required' => 'Tidak boleh kosong',
        ];
    }
}
