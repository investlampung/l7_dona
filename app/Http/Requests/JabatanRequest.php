<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JabatanRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'jabatan'                      => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'jabatan'                      => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'jabatan.required' => 'Tidak boleh kosong'
        ];
    }
}
