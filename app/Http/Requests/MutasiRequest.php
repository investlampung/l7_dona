<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MutasiRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'nip'                      => 'required',
                        'nama'                      => 'required',
                        'asal'                      => 'required',
                        'tujuan'                      => 'required',
                        'tanggal'                      => 'required',
                        'alasan'                      => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'nip'                      => 'required',
                        'nama'                      => 'required',
                        'asal'                      => 'required',
                        'tujuan'                      => 'required',
                        'tanggal'                      => 'required',
                        'alasan'                      => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'nip.required' => 'Tidak boleh kosong',
            'nama.required' => 'Tidak boleh kosong',
            'asal.required' => 'Tidak boleh kosong',
            'tujuan.required' => 'Tidak boleh kosong',
            'tanggal.required' => 'Tidak boleh kosong',
            'alasan.required' => 'Tidak boleh kosong'
        ];
    }
}
