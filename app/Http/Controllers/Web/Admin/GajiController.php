<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gaji;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\GajiRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GajiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $tahun = 2018;
        $pegawai = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        $data = Gaji::orderBy('tahun', 'DESC')->orderBy('id', 'DESC')->get()->all();
        return view('admin.gaji.beranda', compact('data', 'no', 'pegawai', 'tahun'));
    }

    public function create()
    {
        //
    }
    public function cetak(Request $request)
    {
        $tahun = $request->tahun;
        $bulan = $request->bulan;

        $data = Gaji::where('tahun', $tahun)->where('bulan', $bulan)->get();
        // dd($data);

        $no = 1;

        return view('admin.gaji.cetak_bulan', compact('data', 'bulan', 'tahun'));
    }

    public function store(GajiRequest $request)
    {
        $data = $request->all();
        Gaji::create($data);
        $user = User::findOrFail($request->user_id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Gaji " . $user->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Gaji "' . $user->name . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.gaji.index')->with($notification);
    }

    public function show($id)
    {
        $data = Gaji::findOrFail($id);
        return view('admin.gaji.cetak', compact('data'));
    }

    public function edit($id)
    {
        $pegawai = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        $data = Gaji::findOrFail($id);
        return view('admin.gaji.edit', compact('data', 'pegawai'));
    }

    public function update(GajiRequest $request, $id)
    {
        $data = Gaji::findOrFail($id);
        $data->update($request->all());
        $user = User::findOrFail($request->user_id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Gaji " . $user->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Gaji "' . $user->name . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.gaji.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Gaji::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Gaji " . $data->user->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Gaji "' . $data->user->name . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.gaji.index')->with($notification);
    }
}
