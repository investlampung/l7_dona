<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\MutasiRequest;
use App\Mutasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MutasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Mutasi::orderBy('id', 'DESC')->get()->all();
        return view('admin.mutasi.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(MutasiRequest $request)
    {
        $data = $request->all();
        Mutasi::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Mutasi " . $request->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Mutasi "' . $request->nama . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.mutasi.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Mutasi::findOrFail($id);
        return view('admin.mutasi.edit', compact('data'));
    }

    public function update(MutasiRequest $request, $id)
    {
        $data = Mutasi::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Mutasi " . $request->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Mutasi "' . $request->nama . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.mutasi.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Mutasi::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Mutasi " . $data->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Mutasi "' . $data->nama . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.mutasi.index')->with($notification);
    }
}
