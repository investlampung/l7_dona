<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\NaikRequest;
use App\Jabatan;
use App\NaikPangkat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NaikController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $pegawai = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        $data = NaikPangkat::orderBy('id', 'DESC')->get()->all();
        $jabatan = Jabatan::orderBy('jabatan', 'ASC')->get()->all();
        return view('admin.naikjabatan.beranda', compact('data', 'no', 'pegawai', 'jabatan'));
    }

    public function create()
    {
        //
    }

    public function store(NaikRequest $request)
    {
        $data = $request->all();
        NaikPangkat::create($data);
        $pegawai = User::findOrFail($request->user_id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Naik Pangkat " . $pegawai->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Naik Pangkat "' . $pegawai->name . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.naikjabatan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jabatan = Jabatan::orderBy('jabatan', 'ASC')->get()->all();
        $pegawai = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        $data = NaikPangkat::findOrFail($id);
        return view('admin.naikjabatan.edit', compact('data', 'pegawai','jabatan'));
    }

    public function update(NaikRequest $request, $id)
    {
        $data = NaikPangkat::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Naik Pangkat " . $data->user->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Naik Pangkat "' . $data->user->name . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.naikjabatan.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = NaikPangkat::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Naik Pangkat " . $data->user->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Naik Pangkat "' . $data->user->name . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.naikjabatan.index')->with($notification);
    }
}
