<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\JabatanRequest;
use App\Jabatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JabatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Jabatan::orderBy('jabatan', 'ASC')->get()->all();
        return view('admin.jabatan.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(JabatanRequest $request)
    {
        $data = $request->all();
        Jabatan::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Jabatan " . $request->jabatan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Jabatan "' . $request->jabatan . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.jabatan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Jabatan::findOrFail($id);
        return view('admin.jabatan.edit', compact('data'));
    }

    public function update(JabatanRequest $request, $id)
    {
        $data = Jabatan::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Jabatan " . $request->jabatan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Jabatan "' . $request->jabatan . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.jabatan.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Jabatan::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Jabatan " . $data->jabatan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Jabatan "' . $data->jabatan . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.jabatan.index')->with($notification);
    }
}
