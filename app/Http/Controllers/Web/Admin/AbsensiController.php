<?php

namespace App\Http\Controllers\Web\Admin;

use App\Absensi;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\AbsensiRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AbsensiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        return view('admin.absensi.beranda', compact('data', 'no'));
    }
    public function cetak(Request $request)
    {
        $tgl = $request->tanggal.' 00:00:00';

        $data = Absensi::where('tanggal', $tgl)->get();
        // dd($data);

        $no = 1;

        return view('admin.absensi.cetak_hari', compact('data', 'tgl', 'no'));
    }

    public function create()
    {
        $no = 1;
        $data = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        return view('admin.absensi.create', compact('data', 'no'));
    }

    public function store(Request $request)
    {
        $user_id = $request->user_id;
        $tanggal = $request->tanggal;
        $ket = $request->ket;

        foreach ($user_id as $key => $user_value) {
            $userS[] = Absensi::create([
                'user_id' => $user_value,
                'tanggal' => $tanggal[$key],
                'ket' => $ket[$key]
            ]);
        }

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Absensi.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Absensi berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.absensi.index')->with($notification);
    }

    public function show($id)
    {
        $no = 1;
        $data = User::findOrFail($id);
        return view('admin.absensi.show', compact('data', 'no'));
    }

    public function edit($id)
    {
        $data = Absensi::findOrFail($id);
        return view('admin.absensi.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Absensi::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Absensi.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Absensi berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.absensi.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Absensi::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Absensi.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Absensi berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.absensi.index')->with($notification);
    }
}
