<?php

namespace App\Http\Controllers\Web\Admin;

use App\Golongan;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\PegawaiRequest;
use App\Jabatan;
use App\Pegawai;
use App\Pendidikan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PegawaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $jabatan = Jabatan::all();
        $pendidikan = Pendidikan::all();
        $golongan = Golongan::all();
        $data = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        return view('admin.pegawai.beranda', compact('data', 'no', 'pendidikan', 'golongan', 'jabatan'));
    }

    public function create()
    {
        //
    }

    public function store(PegawaiRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt('1sampai2');
        $data['role'] = 'pegawai';

        User::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Pegawai " . $request->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pegawai "' . $request->name . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.pegawai.index')->with($notification);
    }

    public function show($id)
    {
        $data = User::findOrFail($id);
        return view('admin.pegawai.show', compact('data'));
    }

    public function edit($id)
    {
        $jabatan = Jabatan::all();
        $pendidikan = Pendidikan::all();
        $golongan = Golongan::all();
        $data = User::findOrFail($id);
        return view('admin.pegawai.edit', compact('data','jabatan','pendidikan','golongan'));
    }

    public function update(PegawaiRequest $request, $id)
    {
        $data = User::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Pegawai " . $request->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pegawai "' . $request->name . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.pegawai.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Pegawai " . $data->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pegawai "' . $data->name . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.pegawai.index')->with($notification);
    }
}
