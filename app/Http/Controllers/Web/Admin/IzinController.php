<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\IzinRequest;
use App\IzinPegawai;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IzinController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $pegawai = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        $data = IzinPegawai::orderBy('created_at', 'DESC')->get()->all();
        return view('admin.izin.beranda', compact('data', 'no', 'pegawai'));
    }

    public function create()
    {
        //
    }

    public function cetak(Request $request)
    {
        $tahun = $request->tahun;
        $bulan = $request->bulan;

        $data = IzinPegawai::whereRaw('MONTH(created_at) = ?', [$bulan])
            ->whereRaw('YEAR(created_at) = ?', [$tahun])
            ->get();
        // dd($data);
        if ($bulan == 1) {
            $bulan = 'Januari';
        } else if ($bulan == 2) {
            $bulan = 'Februari';
        } else if ($bulan == 3) {
            $bulan = 'Maret';
        } else if ($bulan == 4) {
            $bulan = 'April';
        } else if ($bulan == 5) {
            $bulan = 'Mei';
        } else if ($bulan == 6) {
            $bulan = 'Juni';
        } else if ($bulan == 7) {
            $bulan = 'Juli';
        } else if ($bulan == 8) {
            $bulan = 'Agustus';
        } else if ($bulan == 9) {
            $bulan = 'September';
        } else if ($bulan == 10) {
            $bulan = 'Oktober';
        } else if ($bulan == 11) {
            $bulan = 'November';
        } else if ($bulan == 12) {
            $bulan = 'Desember';
        } else {
        }

        $no = 1;

        return view('admin.izin.cetak', compact('data', 'no', 'bulan', 'tahun'));
    }

    public function store(IzinRequest $request)
    {
        $data = $request->all();
        IzinPegawai::create($data);
        $pegawai = User::findOrFail($request->user_id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Izin " . $pegawai->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Izin "' . $pegawai->name . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.izin.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pegawai = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        $data = IzinPegawai::findOrFail($id);
        return view('admin.izin.edit', compact('data', 'pegawai'));
    }

    public function update(IzinRequest $request, $id)
    {
        $data = IzinPegawai::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Izin " . $data->user->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Izin "' . $data->user->name . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.izin.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = IzinPegawai::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Izin " . $data->user->name;
        History::create($histori);

        $notification = array(
            'message' => 'Data Izin "' . $data->user->name . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.izin.index')->with($notification);
    }
}
