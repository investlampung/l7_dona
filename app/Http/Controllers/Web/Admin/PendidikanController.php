<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\PendidikanRequest;
use App\Pendidikan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PendidikanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Pendidikan::where('pendidikan','not like','-')->orderBy('id', 'ASC')->get()->all();
        return view('admin.pendidikan.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(PendidikanRequest $request)
    {
        $data = $request->all();
        Pendidikan::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Pendidikan " . $request->pendidikan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pendidikan "' . $request->pendidikan . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.pendidikan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Pendidikan::findOrFail($id);
        return view('admin.pendidikan.edit', compact('data'));
    }

    public function update(PendidikanRequest $request, $id)
    {
        $data = Pendidikan::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Pendidikan " . $request->pendidikan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pendidikan "' . $request->pendidikan . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.pendidikan.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Pendidikan::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Pendidikan " . $data->pendidikan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pendidikan "' . $data->pendidikan . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.pendidikan.index')->with($notification);
    }
}
