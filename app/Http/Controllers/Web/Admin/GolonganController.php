<?php

namespace App\Http\Controllers\Web\Admin;

use App\Golongan;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\GolonganRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Command\HistoryCommand;

class GolonganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Golongan::orderBy('golongan', 'ASC')->get()->all();
        return view('admin.golongan.beranda', compact('data', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(GolonganRequest $request)
    {
        $data = $request->all();
        Golongan::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Golongan " . $request->golongan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Golongan "' . $request->golongan . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('admin.golongan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Golongan::findOrFail($id);
        return view('admin.golongan.edit', compact('data'));
    }

    public function update(GolonganRequest $request, $id)
    {
        $data = Golongan::findOrFail($id);
        $data->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Golongan " . $request->golongan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Golongan "' . $request->golongan . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.golongan.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Golongan::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Golongan " . $data->golongan;
        History::create($histori);

        $notification = array(
            'message' => 'Data Golongan "' . $data->golongan . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.golongan.index')->with($notification);
    }
}
