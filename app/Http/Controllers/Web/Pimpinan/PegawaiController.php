<?php

namespace App\Http\Controllers\Web\Pimpinan;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:pimpinan');
    }

    public function index()
    {
        $no = 1;
        $data = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        return view('pimpinan.pegawai.beranda', compact('data', 'no',));
    }
    public function create()
    {
        //
    }
    public function cetak(Request $request, $id)
    {
        $data = User::findOrFail($id);
        return view('pimpinan.pegawai.cetak', compact('data'));
    }
    public function cetak_all()
    {
        $no=1;
        $data = User::where('id', 'not like', 1)->where('id', 'not like', 2)->orderBy('name', 'ASC')->get()->all();
        return view('pimpinan.pegawai.cetak_all', compact('data','no'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::findOrFail($id);
        return view('pimpinan.pegawai.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
