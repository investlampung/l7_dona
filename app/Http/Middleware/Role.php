<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
   
    public function handle($request, Closure $next,$role)
    {
        
        if (Auth::user()->can($role . '-access')) {
            return $next($request);
        }

        return response('Unauthorized.', 401);
    }
}
