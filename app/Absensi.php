<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal'];
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
