<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal_mulai', 'tanggal_selesai'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
